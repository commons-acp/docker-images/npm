FROM node:16-alpine3.14
RUN apk update
RUN apk --no-cache add npm
RUN apk --no-cache add curl
RUN apk --no-cache add wget
RUN apk --no-cache add tar
RUN apk --no-cache add git
RUN apk --update add openssh-client
RUN apk --no-cache add zip
RUN npm install -g release-it


